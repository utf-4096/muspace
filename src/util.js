import { EMAIL_FROM, EMAIL_SIGNATURE } from '../config.js';

export const randomString = (length = 24, alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') => {
    let str = '';

    for(let i = 0; i < length; i++)
        str += alphabet[Math.floor(Math.random() * alphabet.length)];

    return str;
}

export const typeIs = (type, ...values) => {
    let pass = true;

    for(let val of values)
        if(typeof val != type)
            pass = false;

    return pass;
}

export const isID = id => {
    return !isNaN(id) && id >= 1;
}
